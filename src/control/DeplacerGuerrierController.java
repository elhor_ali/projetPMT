package control;

import java.util.Objects;

import constructions.Labyrinthe;
import constructions.Porte;
import constructions.Salle;
import protagonistes.Guerrier;

public class DeplacerGuerrierController 
{
	/********** attributs ************/
	private Labyrinthe labyrinthe;
	private CombattreController combattreController;

	/********** constructeur **********/
	public DeplacerGuerrierController(Labyrinthe labyrinthe, CombattreController combattreController) 
	{
		super();
		this.labyrinthe = labyrinthe;
		this.combattreController = combattreController;
	}

	/*********** gets ***************/
	public Labyrinthe getLabyrinthe() 
	{
		return labyrinthe;
	}


	/************ divers *************/
	public String rentrerDansLeLabyrinthe(String nom) 
	{
		/// \brief : permet de rentrer dans le labyrinthe tout en creant un guerrier
		/// \param[in] nom : le nom du guerrier
		/// \return : une chaine de caractere
		String text = "";
		this.labyrinthe.setGuerrier(new Guerrier(nom));
		text += nom + " rentre dans le labyrinthe.\n";
		text += nom + " rentre dans la salle.";
		return text;
	}

	public String deplacerGuerrier(String dirrection) 
	{
		/// \brief : fonction de deplacement de guerrier selon la dirrection
		/// \param[in] dirrection : la dirrection du déplacement
		/// \return : une chaine de caractere
		String text = "";
		Porte porte = getPorteVers(dirrection);
		if (deplacementPossible(porte)) {
			if (this.combattreController.combatPossible())
				this.combattreController.restaurerEtBoosterMonstre();
			labyrinthe.setEmplacementGuerrier(this.getNouveauEmplacement(Objects.requireNonNull(porte)));
			text += this.labyrinthe.getGuerrier().getNomGuerrier() + " rentre dans la salle.\n";
			if (this.combattreController.combatPossible())
				text += "Un monstre apparait et attaque " + this.labyrinthe.getGuerrier().getNomGuerrier() + ".\n";
		}
		return text;
	}

	private int getNouveauEmplacement(Porte porte) 
	{
		/// \brief : recupere le nouvel emplacement(salle) a partir de la porte passee en param
		/// \param[in] porte : la porte en question
		/// \return : un entier correspendant au nouvel emplacement
		if (porte.getSalle1().equals(labyrinthe.getSalleNumero(labyrinthe.getEmplacementGuerrier()))) {
			return porte.getSalle2().getNumero();
		} else
			return porte.getSalle1().getNumero();
	}

	public boolean deplacementPossible(Porte porte) 
	{
		/// \brief : verifie si un deplacement est possible
		/// \param[in] porte : la porte en question
		/// \return : true si il est possible, false sinon
		if (porte == null)
			return false;
		return true;
	}

	private Porte getPorteVers(String dirrection) 
	{
		/// \brief : récuperer la porte selon la dirrection
		/// \param[in] dirrection : la dirrection du déplacement
		/// \return : une porte
		Porte porte = null;
		if (dirrection.equals("nord"))
			porte = labyrinthe.getSalleNumero(labyrinthe.getEmplacementGuerrier()).getPorteNord();
		else if (dirrection.equals("sud"))
			porte = labyrinthe.getSalleNumero(labyrinthe.getEmplacementGuerrier()).getPorteSud();
		else if (dirrection.equals("est"))
			porte = labyrinthe.getSalleNumero(labyrinthe.getEmplacementGuerrier()).getPorteEst();
		else if (dirrection.equals("ouest"))
			porte = labyrinthe.getSalleNumero(labyrinthe.getEmplacementGuerrier()).getPorteOuest();
		return porte;
	}

	public String[] getListeDeplacements() 
	{
		/// \brief : fonction qui recupere la listes des déplacements possibles
		/// \return : un tableau de chaine de caracteres
		Salle salle = this.labyrinthe.getSalleNumero(this.labyrinthe.getEmplacementGuerrier());
		String[] deplacementsPossibles = new String[5];
		int curseur = 0;
		if (salle.getPorteNord() != null)
			deplacementsPossibles[curseur++] = "nord";
		if (salle.getPorteEst() != null)
			deplacementsPossibles[curseur++] = "est";
		if (salle.getPorteSud() != null)
			deplacementsPossibles[curseur++] = "sud";
		if (salle.getPorteOuest() != null)
			deplacementsPossibles[curseur++] = "ouest";
		deplacementsPossibles[curseur] = "retour";
		return deplacementsPossibles;
	}

	public String salleCourante() 
	{
		/// \brief : recupere la salle dans la quelle le guerrier se trouve actuellement
		/// \return : une chaine de caractere
		int numeroSalleCourante = this.getLabyrinthe().getEmplacementGuerrier();

		return ("salle " + numeroSalleCourante);
	}

	public String guerrierRentreDansLaSalle() 
	{
		/// \brief : fonction qui retourne un dialog d'entree dans une salle
		/// \return : une chaine de caractere
		String nomGuerrier = this.getLabyrinthe().getGuerrier().getNomGuerrier();
		return (nomGuerrier + " rentre dans la salle.");
	}
	
	

}
