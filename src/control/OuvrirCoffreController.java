package control;

import loot.*;
import constructions.Labyrinthe;
import constructions.Salle;
import protagonistes.Guerrier;

public class OuvrirCoffreController 
{
	/*********** attribut ************/
	private Labyrinthe labyrinthe;

	/*********** constructeur ************/
	public OuvrirCoffreController(Labyrinthe labyrinthe) 
	{
		super();
		this.labyrinthe = labyrinthe;
	}

	
	public String ouvrirCoffre() 
	{
		/// \brief : ouvre un coffre dans la salle courante
		/// \return : une chaine de caractere
		Guerrier guerrier = this.labyrinthe.getGuerrier();
		String text = guerrier.getNomGuerrier() + " ouvre le coffre et ";
		Salle salle = this.labyrinthe.getSalleNumero(this.labyrinthe.getEmplacementGuerrier());
		Butin butin = salle.getCoffre().getButin();
		if (butin.getTypeButin() == TypeButin.ARME)
			text += armeTrouvee(butin);
		else if (butin.getTypeButin() == TypeButin.ARMURE)
			text += armureTrouvee(butin);
		else
			text += orTrouve(guerrier, butin);
		return text + "\n";
	}

	public String armeTrouvee(Butin butin) 
	{
		/// \brief : renvoie une chaine de caractere pour une arme trouvee
		/// \param[in] Butin : butin en question (arme, armure ou or)
		/// \return : une chaine de caractere
		String text = "trouve une arme de rareté " + butin.getRarete() + ".";
		text += "\nVoulez-vous récuperer cet Arme?";
		return text;
	}

	public String armureTrouvee(Butin butin) 
	{
		/// \brief : renvoie une chaine de caractere pour une armure trouvee
		/// \param[in] Butin : butin en question (arme, armure ou or)
		/// \return : une chaine de caractere
		String text = "trouve une armure de rareté " + butin.getRarete() + ".";
		text += "\nVoulez-vous récuperer cet Armure?";
		return text;
	}

	public String orTrouve(Guerrier guerrier, Butin butin) 
	{
		/// \brief : renvoie une chaine de caractere et donne l'or au guerrier
		/// \param[in] guerrier : guerrier qui reçoit l'or
		/// \param[in] butin : butin en question (arme, armure ou or)
		/// \return : une chaine de caractere
		Or or = (Or) butin;
		String text = "trouve et récupere " + or.getQuantite() + " piéces d'or.";
		guerrier.setOr(or.getQuantite() + guerrier.getOr());
		return text;
	}

	public String equiperArmeOuArmure(boolean equip) 
	{
		/// \brief : renvoie une chaine de caractere apres avoir avoir equiper une arme/armure
		/// \param[in] equip : boolean qui indique si le guerrier a choisi d'equiper ou pas le butin
		/// \return : une chaine de caractere
		Guerrier guerrier = this.labyrinthe.getGuerrier();
		Salle salle = this.labyrinthe.getSalleNumero(this.labyrinthe.getEmplacementGuerrier());
		Butin butin = salle.getCoffre().getButin();
		if (equip) {
			if (butin instanceof Arme)
				guerrier.setArme((Arme) butin);
			else
				guerrier.setArmure((Armure) butin);
			return guerrier.getNomGuerrier() + " a récuperé l'equipement.\n";
		} else
			return guerrier.getNomGuerrier() + " n'a pas récuperé l'equipement.\n";
	}

	public int rareteButin() 
	{
		/// \brief : renvoie la rarete d'un butin
		/// \return : un int
		Salle salle = this.labyrinthe.getSalleNumero(this.labyrinthe.getEmplacementGuerrier());
		Butin butin = salle.getCoffre().getButin();
		return butin.getRarete();
	}

	public String typeButin() 
	{
		/// \brief : renvoie la type du butin sous forme de chaine de caracteres
		/// \return : une chaine de caractere
		Salle salle = this.labyrinthe.getSalleNumero(this.labyrinthe.getEmplacementGuerrier());
		Butin butin = salle.getCoffre().getButin();
		if (butin.getTypeButin() == TypeButin.ARME)
			return "arme";
		else if (butin.getTypeButin() == TypeButin.ARMURE)
			return "armure";
		else
			return "or";
	}

	public int orGuerrier() 
	{
		/// \brief : renvoie la quantité d'or detenue par le guerrier
		/// \return : un entier correspondant la l'or
		return this.labyrinthe.getGuerrier().getOr();
	}

}
