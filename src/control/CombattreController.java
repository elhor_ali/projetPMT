package control;

import java.util.Random;

import constructions.Labyrinthe;
import protagonistes.Guerrier;
import protagonistes.Monstre;

public class CombattreController 
{
	/*************** attributs ***************/
	private Labyrinthe labyrinthe;
	private OuvrirCoffreController ouvrirCoffreController;

	
	/*************** constructeur ************/
	public CombattreController(Labyrinthe labyrinthe, OuvrirCoffreController ouvrirCoffreController) 
	{
		super();
		this.labyrinthe = labyrinthe;
		this.ouvrirCoffreController = ouvrirCoffreController;
	}


	
	/************** gets **************/
	public int getGuerrierHealth() {
		return this.labyrinthe.getGuerrier().getPointsDeVie();
	}

	public int getGuerrierHealthMax() {
		return this.labyrinthe.getGuerrier().getPointsDeVieMax();
	}

	public int getGuerrierShield() {
		return this.labyrinthe.getGuerrier().getArmure().getPuissance();
	}

	public int getGuerrierShieldMax() {
		return this.labyrinthe.getGuerrier().getArmure().getPuissanceMax();
	}

	public int getMonstreHealth() {
		return this.labyrinthe.getSalleNumero(this.labyrinthe.getEmplacementGuerrier()).getMonstre().getPointsDeVie();
	}

	public int getMonstreHealthMax() {
		return this.labyrinthe.getSalleNumero(this.labyrinthe.getEmplacementGuerrier()).getMonstre()
				.getPointsDeVieMax();
	}
	
	/************** divers ****************/
	public String tourDattaque() 
	{
		/// \brief : effectue un tour d'attaque 
		/// \return : une chaine de caracteres à lire par l'écran
		String text = "";
		Guerrier guerrier = this.labyrinthe.getGuerrier();
		Monstre monstre = labyrinthe.getSalleNumero(this.labyrinthe.getEmplacementGuerrier()).getMonstre();
		int choix = this.generateNumber(0, 1);
		if (choix == 0)
			text += coupDeMonstre(guerrier, monstre);
		else
			text += coupDeGuerrier(monstre, guerrier);
		return text;
	}
	
	
	public String coupDeMonstre(Guerrier guerrier, Monstre monstre) 
	{
		/// \brief : effectue une attaque du monstre
		/// \param[in] guerrier : le guerrier qui prends le coup
		/// \param[in] monstre : le monstre qui donne le coup
		/// \return : une chaine de caracteres à lire par l'ecran
		monstre.donnerCoup(guerrier);
		if (guerrier.getPointsDeVie() == 0) {
			restaurerEtBoosterMonstre();
			return this.labyrinthe.getGuerrier().getNomGuerrier() + " est mort.\n";
		}
		return "";
	}

	public String coupDeGuerrier(Monstre monstre, Guerrier guerrier) 
	{
		/// \brief : effectue une attaque du guerrier
		/// \param[in] monstre : le guerrier qui prends le coup
		/// \param[in] guerrier : le guerrier qui donne le coup
		/// \return : une chaine de caracteres à lire par l'ecran
		guerrier.donnerCoup(monstre);
		String text;
		if (monstre.getPointsDeVie() == 0) {
			if (this.guerrierGagneLaPartie())
			{
				text = "Le boss des monstres est mort.\n";
				text+=this.labyrinthe.getGuerrier().getNomGuerrier()+" triomphe et sort du labyrinthe.\n";
			}
			else
			{
				restaurerEtBoosterGuerrier();
				text = "Le monstre est mort.\n";
				text += "Un coffre vient d'aparaitre.\n";
				text += this.ouvrirCoffreController.ouvrirCoffre();
			}
			return text;
		}
		return "";
	}

	public int generateNumber(int min, int max) 
	{
		/// \brief : permet de generer un nombre entre min et max
		/// \param[in] min : le nombre min
		/// \param[in] max : le nombre max
		/// \return : une entier entre min et max
		return new Random().nextInt(max - min + 1) + min;
	}

	public boolean combatPossible() 
	{
		/// \brief : retourne true si un combat est possible
		/// \return : true si un combat est possible, false sinon
		Guerrier guerrier = this.labyrinthe.getGuerrier();
		Monstre monstre = labyrinthe.getSalleNumero(this.labyrinthe.getEmplacementGuerrier()).getMonstre();
		if (monstre != null && guerrier != null && monstre.getPointsDeVie() > 0 && guerrier.getPointsDeVie() > 0) {
			return true;
		}
		return false;
	}

	public String estMort() 
	{
		/// \brief : retourne un chaine de caractere si le personnage meurt
		/// \return : une chaine de caractere
		Guerrier guerrier = this.labyrinthe.getGuerrier();
		Monstre monstre = labyrinthe.getSalleNumero(this.labyrinthe.getEmplacementGuerrier()).getMonstre();
		if (guerrier.getPointsDeVie() == 0)
			return "guerrier";
		else if (monstre.getPointsDeVie() == 0)
			return "monstre";
		else
			return null;
	}

	public void restaurerEtBoosterMonstre() 
	{
		/// \brief : restaure la vie d'un monstre et lui procure 1 point de vie max en plus
		/// \return : void
		Monstre monstre = this.labyrinthe.getSalleNumero(this.labyrinthe.getEmplacementGuerrier()).getMonstre();
		monstre.setPointsDeVieMax(monstre.getPointsDeVieMax() + 1);
		monstre.setPointsDeVie(monstre.getPointsDeVieMax());
	}

	public void restaurerEtBoosterGuerrier() 
	{
		/// \brief : restaure la vie d'un guerrier et lui procure 1 point de vie max en plus
		/// \return : void
		Guerrier guerrier = this.labyrinthe.getGuerrier();
		guerrier.setPointsDeVieMax(guerrier.getPointsDeVieMax() + 1);
		guerrier.setPointsDeVie(guerrier.getPointsDeVieMax());
	}

	
	public boolean guerrierGagneLaPartie()
	{
		/// \brief : verifie si le guerrier gagne la partie
		/// \return : true si le guerrier gagne, false sinon
		if(this.labyrinthe.guerrierIsDerniereSalle() && this.getMonstreHealth()==0)
		{
			return true;
		}
		return false;
	}
	

}
