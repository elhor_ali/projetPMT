package protagonistes;

abstract public class Combattant 
{
	/********* attributs ***********/
	protected int pointsDeVieMax;
	protected int pointsDeVie;

	/********* constructeur **********/
	public Combattant(int pointsDeVieMax) 
	{
		this.pointsDeVie = pointsDeVieMax;
		this.pointsDeVieMax = pointsDeVieMax;
	}

	/*********** gets ***********/
	public int getPointsDeVie() // à modifier apres pour protected
	{
		return pointsDeVie;
	}

	public int getPointsDeVieMax() {
		return pointsDeVieMax;
	}
	
	/************ sets **************/
	public void setPointsDeVie(int pointsDeVie) // pour tester
	{
		this.pointsDeVie = pointsDeVie;
	}

	public void setPointsDeVieMax(int pointsDeVieMax) {
		this.pointsDeVieMax = pointsDeVieMax;
	}
	
	/************* divers *************/
	public void donnerCoup(Combattant combattant) 
	{
		combattant.prendreCoup(1);
	}

	public void prendreCoup(int puissanceCoup) 
	{
		if (this.pointsDeVie - puissanceCoup <= 0) {
			this.pointsDeVie = 0;
		} else
			this.pointsDeVie -= puissanceCoup;
	}

}
