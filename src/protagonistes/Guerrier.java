package protagonistes;

import loot.Arme;
import loot.Armure;

public class Guerrier extends Combattant {
	private String nomGuerrier;
	private Arme arme;
	private Armure armure;
	private int or;

	public int getOr() {
		return or;
	}

	public void setOr(int or) {
		this.or = or;
	}

	public Guerrier(String nomGuerrier) {
		super(10);
		this.nomGuerrier = nomGuerrier;
		this.arme = null;
		this.armure = null;
		this.or = 0;
	}

	public String getNomGuerrier() {
		return nomGuerrier;
	}

	public void equiperArmure(Arme arme) {
		this.arme = arme;
	}

	public void equiperArmure(Armure armure) {
		this.armure = armure;
	}

	@Override
	public void donnerCoup(Combattant combattant) {
		int puissanceCoup = 1;
		if (arme != null)
			puissanceCoup += arme.getPuissance();
		combattant.prendreCoup(puissanceCoup);
	}

	@Override
	public void prendreCoup(int puissanceCoup) {
		if (this.armure != null) {
			if (this.armure.getPuissance() - puissanceCoup <= 0) {
				puissanceCoup = puissanceCoup - this.armure.getPuissance();
				this.armure.setPuissance(0);
			} else {
				this.armure.setPuissance(this.armure.getPuissance() - puissanceCoup);
				puissanceCoup = 0;
			}
		}
		if (this.pointsDeVie - puissanceCoup <= 0) {
			this.pointsDeVie = 0;
		} else
			this.pointsDeVie -= puissanceCoup;
	}

	public Arme getArme() {
		return arme;
	}

	public void setArme(Arme arme) {
		this.arme = arme;
	}

	public Armure getArmure() {
		return armure;
	}

	public void setArmure(Armure armure) {
		this.armure = armure;
	}

}
