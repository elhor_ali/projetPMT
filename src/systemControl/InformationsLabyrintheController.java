package systemControl;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

/* une classe qui permet de recuperer des informations pour la construction du labyrinthe */
public class InformationsLabyrintheController {
	/* valeurs par defaut */
	private int nombreDeSalles = 11;
	private String[][] donneesPortes = { 
			{ "0", "nord", "1", "sud", "false" }, { "0", "est", "2", "ouest", "false" },
			{ "2", "est", "3", "ouest", "false" }, { "3", "nord", "4", "sud", "false" },
			{ "4", "ouest", "5", "est", "false" }, { "4", "nord", "6", "sud", "false" },
			{ "4", "est", "7", "ouest", "false" }, { "7", "est", "8", "ouest", "false" },
			{ "8", "sud", "9", "nord", "false" }, { "9", "sud", "10", "nord", "false" } };

	public int getNombreSalles() {
		return this.nombreDeSalles;
	}

	public String[][] getDonneesPortes() {
		return this.donneesPortes;
	}

	public void lectureFichier(
			String nomFichierLabyrinthe) { /* mise à jour des valeur au cas d'une lecture fichier réusit */
		try {
			FileInputStream fichierLabyrinthe = new FileInputStream(nomFichierLabyrinthe);
			Scanner scanner = new Scanner(fichierLabyrinthe);

			int nombrePortes = 0;
			if (scanner.hasNextLine())
				this.nombreDeSalles = Integer.parseInt(scanner.nextLine());
			if (scanner.hasNextLine())
				nombrePortes = Integer.parseInt(scanner.nextLine());
			String[][] donneesPortesTemp = new String[nombrePortes][5];
			int indiceParcours = 0;
			while (scanner.hasNextLine()) {
				donneesPortesTemp[indiceParcours] = scanner.nextLine().split(",");
				indiceParcours++;
			}
			scanner.close();
			this.donneesPortes = donneesPortesTemp;
		} catch (IOException e) {
			System.err.println(e);
		}
	}
}
