package systemControl;

import loot.Coffre;
import constructions.Labyrinthe;
import constructions.Porte;
import constructions.Salle;
import protagonistes.Monstre;

/* classe qui creer le labyrinthe */
public class CreationLabyrintheController {
	private Labyrinthe labyrinthe;
	private InformationsLabyrintheController informationsLabyrinthe;

	public CreationLabyrintheController(Labyrinthe labyrinthe,
			InformationsLabyrintheController informationsLabyrinthe) { // constructeur
		this.labyrinthe = labyrinthe;
		this.informationsLabyrinthe = informationsLabyrinthe;
	}

	public void creerLabyrinthe() { // la fonction de creation du labyrinthe
		int nombreSalles = informationsLabyrinthe.getNombreSalles();
		String[][] donneesPortes = informationsLabyrinthe.getDonneesPortes();
		creerLesSalles(nombreSalles);
		creerLesPortes(donneesPortes);
		ajouterLesMonstres();
		ajouterLesCoffres();
	}

	private void creerLesSalles(int nombreDeSalles) { // la fonction qui creer les salles
		for (int i = 0; i < nombreDeSalles; i++) {
			this.labyrinthe.ajouterSalle(new Salle(i));
		}
	}

	private void creerLesPortes(String[][] donneesPortes) { // la fonction qui permet de creer les portes
		Salle salle1;
		Salle salle2;
		String emplacementPorteDansSalle1;
		String emplacementPorteDansSalle2;
		Porte nouvellePorte;

		for (String[] donneesPorte : donneesPortes) {
			salle1 = labyrinthe.getSalleNumero(Integer.parseInt(donneesPorte[0]));
			emplacementPorteDansSalle1 = donneesPorte[1];
			salle2 = labyrinthe.getSalleNumero(Integer.parseInt(donneesPorte[2]));
			emplacementPorteDansSalle2 = donneesPorte[3];
			nouvellePorte = new Porte(salle1, salle2, Boolean.parseBoolean(donneesPorte[4]));
			this.labyrinthe.ajouterPorte(nouvellePorte);
			relierPorteASalle(nouvellePorte, salle1, emplacementPorteDansSalle1);
			relierPorteASalle(nouvellePorte, salle2, emplacementPorteDansSalle2);
		}
	}

	private void relierPorteASalle(Porte porte, Salle salle, String emplacement) { // la fonction qui relie les porte au
																					// salles
		if (emplacement.equals("nord"))
			salle.setPorteNord(porte);
		else if (emplacement.equals("est"))
			salle.setPorteEst(porte);
		else if (emplacement.equals("sud"))
			salle.setPorteSud(porte);
		else if (emplacement.equals("ouest"))
			salle.setPorteOuest(porte);
	}

	public void ajouterLesMonstres() {
		int i;
		for (i = 0; i < informationsLabyrinthe.getNombreSalles() - 2; i++) {
			labyrinthe.getSalleNumero(i + 1).setMonstre(new Monstre(5));
		}
		labyrinthe.getSalleNumero(i + 1).setMonstre(new Monstre(15));
	}

	public void ajouterLesCoffres() {
		int i;
		for (i = 0; i < informationsLabyrinthe.getNombreSalles() - 2; i++) {
			labyrinthe.getSalleNumero(i + 1).setCoffre(new Coffre());
		}
	}

}
