package tests;

import constructions.Labyrinthe;
import control.CombattreController;
import control.DeplacerGuerrierController;
import control.OuvrirCoffreController;
import fenetre.FenetreManager;
import systemControl.CreationLabyrintheController;
import systemControl.InformationsLabyrintheController;

public class testVisu {
	public static void main(String[] args) {
		Labyrinthe labyrinthe = new Labyrinthe(); // declaration de laby

		InformationsLabyrintheController informationsLabyrinthe = new InformationsLabyrintheController(); // initialiser
																											// les
																											// données
																											// du
																											// labyrinthe
																											// a des
																											// valeurs
																											// par
																											// defaut
		informationsLabyrinthe.lectureFichier("ressources/levels/1.txt"); // lecture d'un fichier de niveau

		CreationLabyrintheController creationLabyrintheController = new CreationLabyrintheController(labyrinthe,
				informationsLabyrinthe); // declaration de la creation du laby
		creationLabyrintheController.creerLabyrinthe(); // creation du laby

		OuvrirCoffreController ouvrirCoffreController = new OuvrirCoffreController(labyrinthe);

		CombattreController combattreController = new CombattreController(labyrinthe, ouvrirCoffreController);

		DeplacerGuerrierController deplacerGuerrierController = new DeplacerGuerrierController(labyrinthe,
				combattreController);

		new FenetreManager(deplacerGuerrierController, combattreController, ouvrirCoffreController);
	}
}
