package constructions;

import loot.Coffre;
import protagonistes.Monstre;

public class Salle {

	/****************** attributs *******************/
	private int numero;
	private Monstre monstre;
	private Coffre coffre;
	private Porte porteNord;
	private Porte porteSud;
	private Porte porteEst;
	private Porte porteOuest;

	/****************** constructeur ****************/
	public Salle(int numero) {
		this.numero = numero;
		this.monstre = null;
		this.porteNord = null;
		this.porteSud = null;
		this.porteEst = null;
		this.porteOuest = null;
		this.coffre = null;
	}

	/***************** gets *****************/
	public Porte getPorteNord() {
		return porteNord;
	}

	public Porte getPorteSud() {
		return porteSud;
	}

	public Porte getPorteEst() {
		return porteEst;
	}

	public Porte getPorteOuest() {
		return porteOuest;
	}

	public Monstre getMonstre() {
		return monstre;
	}

	public int getNumero() {
		return numero;
	}

	/***************** sets *****************/
	public void setPorteNord(Porte porteNord) {
		this.porteNord = porteNord;
	}

	public void setPorteSud(Porte porteSud) {
		this.porteSud = porteSud;
	}

	public void setPorteEst(Porte porteEst) {
		this.porteEst = porteEst;
	}

	public void setPorteOuest(Porte porteOuest) {
		this.porteOuest = porteOuest;
	}

	public void setMonstre(Monstre monstre) {
		this.monstre = monstre;
	}

	public Coffre getCoffre() {
		return coffre;
	}

	public void setCoffre(Coffre coffre) {
		this.coffre = coffre;
	}

}
