package constructions;

import java.util.ArrayList;

import protagonistes.Guerrier;

public class Labyrinthe {
	/*********** attributs **************/
	private Guerrier guerrier;
	private ArrayList<Salle> salles = new ArrayList<Salle>(); /* la liste des pièces */
	private ArrayList<Porte> portes = new ArrayList<Porte>(); /* la liste des portes */
	private int emplacementGuerrier; /* le numéro de salle ou se trouve le guerrier */

	/*********** ajout dans listes **********/
	public void ajouterSalle(Salle salle) 
	{
		this.salles.add(salle);
	}

	public void ajouterPorte(Porte porte) 
	{
		this.portes.add(porte);
	}
	/****************************************/

	/************* gets **************/
	public Guerrier getGuerrier() {
		return guerrier;
	}

	public int getEmplacementGuerrier() {
		return emplacementGuerrier;
	}

	public ArrayList<Salle> getSalles() {
		return salles;
	}

	public void setSalles(ArrayList<Salle> salles) {
		this.salles = salles;
	}

	/*************** sets *************/
	public void setEmplacementGuerrier(int emplacementGuerrier) {
		this.emplacementGuerrier = emplacementGuerrier;
	}

	public void setGuerrier(Guerrier guerrier) 
	{
		this.guerrier = guerrier;
		this.emplacementGuerrier = 0;
	}
	/**********************************/

	
	/***** divers ********/
	public Salle getSalleNumero(int numero) 
	{
		/// \brief : recupere une salle de numero "numero" 
		/// \param[in] numero : le numero de la salle
		/// \return : la Salle
		for (Salle salle : salles) {
			if (salle.getNumero() == numero)
				return salle;
		}
		return salles.get(0);
	}
	
	public boolean guerrierIsDerniereSalle()
	{
		/// \brief : verifie si le guerrier est à la derniere salle
		/// \return : true si le guerrier est à la derniere salle, false sinon
		if(this.emplacementGuerrier==this.salles.size()-1) return true;
		else return false;
	}
	/**********************/

}
