package constructions;

public class Porte {
	/************* attributs ****************/
	private Salle salle1;
	private Salle salle2;
	private boolean estVerrouillee;
	
	
	/**************** constructeur ***************/
	public Porte(Salle salle1, Salle salle2, boolean estVerrouillee) 
	{
		this.salle1 = salle1;
		this.salle2 = salle2;
		this.estVerrouillee = estVerrouillee;
	}

	/***************** gets *******************/
	public boolean isEstVerrouillee() 
	{
		return estVerrouillee;
	}
	
	public Salle getSalle1() {
		return salle1;
	}

	public Salle getSalle2() {
		return salle2;
	}

}
