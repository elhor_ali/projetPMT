package fenetre;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class BarreActionPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JButton[] actions = new JButton[6];
	private FenetreManager fenetreController;

	public BarreActionPanel(FenetreManager fenetreController) {
		this.setPreferredSize(new Dimension(900, 150));
		this.setBackground(Color.BLACK);
		this.setLayout(new GridLayout(2, 3));
		this.initMenu();
		this.setBorder(BorderFactory.createLineBorder(Color.WHITE));
		this.fenetreController = fenetreController;
	}

	public void initMenu() {
		for (int i = 0; i < 6; i++) {
			this.actions[i] = new JButton();
			this.actions[i].setBackground(Color.BLACK);
			this.actions[i].setForeground(Color.WHITE);
			this.actions[i].setFont(new Font("SF Pixelate", Font.BOLD, 24));
			this.actions[i].setBorder(BorderFactory.createEmptyBorder());
			this.actions[i].setFocusPainted(false);
			this.actions[i].setContentAreaFilled(false);
			this.actions[i].setHorizontalAlignment(SwingConstants.CENTER);
			this.actions[i].addActionListener(this);
			this.add(this.actions[i]);
		}
	}

	public void setActions(String[] nomActions) {
		for (int i = 0; i < nomActions.length; i++) {
			this.actions[i].setText(nomActions[i]);
			this.actions[i].setActionCommand(nomActions[i]);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		fenetreController.performAction(action);
	}

}
