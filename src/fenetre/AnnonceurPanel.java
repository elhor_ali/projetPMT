package fenetre;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.Timer;

public class AnnonceurPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PossessionsGuerrier possessionsGuerrier;
	private JLabel jLabel;
	private JTextArea jTextArea;
	private String text;

	public AnnonceurPanel(PossessionsGuerrier possessionsGuerrierTemp) {
		super();
		this.possessionsGuerrier = possessionsGuerrierTemp;
		init();
		this.add(jLabel, BorderLayout.NORTH);
		this.add(jTextArea, BorderLayout.CENTER);
		this.add(possessionsGuerrier, BorderLayout.SOUTH);
	}

	public void init() {// initialisations diverses
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(900, 320));
		this.setBackground(Color.BLACK);
		this.setBorder(null);
		this.initJLabel();
		this.initJTextArea();
		this.setEnabled(false);
	}

	public void initJLabel() {
		this.jLabel = new JLabel();
		this.jLabel.setPreferredSize(new Dimension(900, 40));
		this.jLabel.setBackground(Color.BLACK); // la couleur du fond
		this.jLabel.setForeground(Color.WHITE); // la couleur de la police
		this.jLabel.setFont(new Font("SF Pixelate", Font.PLAIN, 30));
		this.jLabel.setHorizontalAlignment(SwingConstants.CENTER);
	}

	public void initJTextArea() {
		this.jTextArea = new JTextArea();
		jTextArea.setPreferredSize(new Dimension(200, 100));
		this.jTextArea.setBackground(Color.BLACK); // la couleur du fond
		this.jTextArea.setForeground(Color.WHITE); // la couleur de la police
		this.jTextArea.setFont(new Font("SF Pixelate", Font.PLAIN, 20));
		this.jTextArea.setCaret(new MyCaret());
		this.jTextArea.setEditable(false);
		jTextArea.setLineWrap(true);
		jTextArea.setWrapStyleWord(true);
		jTextArea.setMargin(new Insets(0, 20, 0, 20));
	}

	public void afficherTitre(String text) {
		this.jLabel.setText(text);

	}

	public void afficher(String text) {
		if (!text.equals("")) {
			if (this.text == null)
				this.text = text;
			else
				this.text += text;
			this.timer.start();
		}
	}

	public void cleanJTextArea() {
		this.text = null;
		i = 0;
		this.jTextArea.setText(null);
	}

	/*** timer ***/
	private int i = 0;
	private Timer timer = new Timer(15, new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			char character[] = text.toCharArray();
			int arrayNumber = character.length;

			String s = String.valueOf(character[i]);

			jTextArea.append(s);

			i++;

			if (i == arrayNumber) {
				timer.stop();
			}
		}
	});
	/*************/
}
