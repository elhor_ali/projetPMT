package fenetre;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class DemanderNomPanel extends JSplitPane {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel textFielContainer;
	private JTextField jTextField;
	private JLabel jLabel;

	public DemanderNomPanel() {
		super(VERTICAL_SPLIT);
		this.init();
		this.adds();
	}

	public void init() {// initialisations diverses
		this.setPreferredSize(new Dimension(900, 320));
		this.setBackground(Color.BLACK);
		this.setBorder(null);
		this.initJTextArea();
		this.initJLabel();
		this.setResizeWeight(0.5);
		this.setEnabled(false);
		this.setDividerSize(0);
	}

	public void adds() {// ajout des differentes components
		this.add(jLabel);
		this.textFielContainer = new JPanel(new BorderLayout());
		this.textFielContainer.setBackground(Color.BLACK);
		this.textFielContainer.add(jTextField, BorderLayout.NORTH);
		this.add(textFielContainer);
	}

	public void initJTextArea() {
		this.jTextField = new JTextField(10);
		jTextField.setSize(new Dimension(200, 100));
		jTextField.setBorder(null);
		this.jTextField.setBackground(Color.BLACK); // la couleur du fond
		this.jTextField.setForeground(Color.WHITE); // la couleur de la police
		this.jTextField.setFont(new Font("SF Pixelate", Font.PLAIN, 24));
		this.jTextField.setHorizontalAlignment(SwingConstants.CENTER);
		this.jTextField.setCaret(new MyCaret());
	}

	public void initJLabel() {
		this.jLabel = new JLabel();
		this.jLabel.setBackground(Color.BLACK); // la couleur du fond
		this.jLabel.setForeground(Color.WHITE); // la couleur de la police
		this.jLabel.setFont(new Font("SF Pixelate", Font.BOLD, 26));
		this.jLabel.setText("Choisissez un nom pour votre guerrier");
		this.jLabel.setHorizontalAlignment(SwingConstants.CENTER);
	}

	public String getNomGuerrier() {
		return this.jTextField.getText();
	}

}
