package fenetre;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JProgressBar;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;

public class InfosMonstre extends JSplitPane {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JProgressBar monstre;

	public InfosMonstre() {
		super(VERTICAL_SPLIT);
		this.setPreferredSize(new Dimension(20, 320));
		this.setResizeWeight(0.999);
		this.setEnabled(false);
		this.setDividerSize(0);
		this.setBorder(null);
		this.initMonstre();
		this.add(monstre);
	}

	public void initMonstre() {
		monstre = new JProgressBar(SwingConstants.VERTICAL, 0, 5);
		monstre.setForeground(new Color(175, 17, 7));
		monstre.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2, true));
		monstre.setToolTipText("MONSTER");
		monstre.setBackground(Color.BLACK);
		this.setBackground(Color.BLACK);
	}

	public void setMonsterMaxHealth(int max) {
		this.monstre.setMaximum(max);
	}

	public void setMonsterCurrentHealth(int health) {
		this.monstre.setValue(health);
	}

}
