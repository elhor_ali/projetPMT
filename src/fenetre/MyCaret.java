package fenetre;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.JTextComponent;

public class MyCaret extends DefaultCaret {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mark = "_";

	public MyCaret() {
		setBlinkRate(666);
	}

	@Override
	protected synchronized void damage(Rectangle r) {
		if (r == null)
			return;

		JTextComponent comp = getComponent();
		FontMetrics fm = comp.getFontMetrics(comp.getFont());
		int textWidth = fm.stringWidth(">");
		int textHeight = fm.getHeight();
		x = r.x;
		y = r.y;
		width = textWidth;
		height = textHeight;
		repaint(); // calls getComponent().repaint(x, y, width, height)
	}

	@SuppressWarnings("deprecation")
	@Override
	public void paint(Graphics g) {
		JTextComponent comp = getComponent();
		if (comp == null) {
			return;
		}

		int dot = getDot();
		Rectangle r = null;
		try {
			r = comp.modelToView(dot);
		} catch (BadLocationException e) {
			return;
		}
		if (r == null)
			return;

		if ((x != r.x) || (y != r.y)) {
			repaint(); // erase previous location of caret
			damage(r);
		}

		if (isVisible()) {
			FontMetrics fm = comp.getFontMetrics(comp.getFont());
			g.setColor(Color.WHITE);
			g.drawString(mark, x + 2, y + fm.getAscent() + 3);
		}
	}

}
