package fenetre;

import control.CombattreController;
import control.DeplacerGuerrierController;
import control.OuvrirCoffreController;

/* une clase qui controle l'entrée souris */
public class FenetreManager {
	private StockageContainers stockageContainers;
	private DeplacerGuerrierController deplacerGuerrierController; // un controller deplacerGuerrer
	private CombattreController combattreController;
	private OuvrirCoffreController ouvrirCoffreController;

	/* menus d'actions de base */

	public FenetreManager(DeplacerGuerrierController deplacerGuerrierController,
			CombattreController combattreController, OuvrirCoffreController ouvrirCoffreController) {
		super();
		this.stockageContainers = new StockageContainers(this);
		this.deplacerGuerrierController = deplacerGuerrierController;
		this.combattreController = combattreController;
		this.ouvrirCoffreController = ouvrirCoffreController;
		this.stockageContainers.getFenetre().setVisible(true);
		this.init();
	}

	public void init() {
		this.stockageContainers.getBarreActionPanel().setActions(actionsSuivantes("creation guerrier"));
		this.stockageContainers.getFenetre().setConfiguration(this.stockageContainers.getDemanderNomPanel(),
				this.stockageContainers.getBarreActionPanel());
	}

	public void performAction(String action) {
		/// \brief : permet d'effectuer une action et d'afficher le resultat sur la
		/// fenetre
		/// \param[in] action: la chaine de caractere buffer remplir
		/// \return void
		if (action == "retour")
			actionRetour();
		else if (action == "se déplacer" || action == "fuir")
			actionSeDeplacer();
		else if (action == "attaquer")
			actionAttaquer();
		else if (action == "equipement")
			autresActions();
		else if (action == "sauvegarder")
			autresActions();
		else if (action == "quitter")
			autresActions();
		else if (action == "valider")
			actionValider();
		else if (action == "oui")
			actionOui();
		else if (action == "non")
			actionNon();
		else
			actionDeplacement(action);
		this.setConfig();
	}

	public void actionOui() {
		/// \brief : permet d'effectuer l'action de 'oui'
		/// \return void
		this.fflush();
		this.stockageContainers.getBarreActionPanel().setActions(this.actionsSuivantes("oui"));
		String text = this.ouvrirCoffreController.equiperArmeOuArmure(true);

		if (this.ouvrirCoffreController.typeButin().equals("arme"))
			this.stockageContainers.getPossessionsGuerrier().setArme(this.ouvrirCoffreController.rareteButin());
		else if (this.ouvrirCoffreController.typeButin().equals("armure"))
			this.stockageContainers.getPossessionsGuerrier().setArmure(this.ouvrirCoffreController.rareteButin());

		this.stockageContainers.getAnnonceurPanel().afficher(text);
		majBarreDeProtectionGuerrier();
	}

	public void actionNon() {
		/// \brief : permet d'effectuer l'action de 'non'
		/// \return void
		this.fflush();
		this.stockageContainers.getBarreActionPanel().setActions(this.actionsSuivantes("non"));
		String text = this.ouvrirCoffreController.equiperArmeOuArmure(false);
		this.stockageContainers.getAnnonceurPanel().afficher(text);

	}

	public void setConfig() {

		if (this.combattreController.combatPossible()) {
			this.stockageContainers.getFenetre().changeLeftComponent(this.stockageContainers.getInfosMonstre());
		} else {
			this.stockageContainers.getFenetre()
					.changeLeftComponent(this.stockageContainers.getRemplacementInfosMonstre());
		}
	}

	private void actionRetour() {
		/// \brief : permet d'effectuer l'action de 'retour'
		/// \return void
		this.fflush();
		this.stockageContainers.getBarreActionPanel().setActions(this.actionsSuivantes("retour"));
	}

	private void actionSeDeplacer() {
		/// \brief : permet d'effectuer l'action de 'se deplacer'
		/// \return void
		this.fflush();
		this.stockageContainers.getBarreActionPanel().setActions(deplacerGuerrierController.getListeDeplacements());
	}

	private void actionValider() {
		/// \brief : permet d'effectuer l'action de 'valider'
		/// \return void
		this.fflush();
		this.stockageContainers.getBarreActionPanel().setActions(this.actionsSuivantes("valider"));

		String nomGuerrier = this.stockageContainers.getDemanderNomPanel().getNomGuerrier();
		String text = this.deplacerGuerrierController.rentrerDansLeLabyrinthe(nomGuerrier);

		this.stockageContainers.getAnnonceurPanel().afficherTitre(this.deplacerGuerrierController.salleCourante());
		this.stockageContainers.getAnnonceurPanel().afficher(text);

		majBarreDeVieGuerrier();

		this.stockageContainers.getFenetre().setConfiguration(this.stockageContainers.getAnnonceurPanel(),
				this.stockageContainers.getBarreActionPanel(), this.stockageContainers.getInfosGuerrierPanel(),
				this.stockageContainers.getRemplacementInfosMonstre());
	}

	private void actionAttaquer() {
		/// \brief : permet d'effectuer l'action de 'attaquer'
		/// \return void

		String text = this.combattreController.tourDattaque();
		this.stockageContainers.getAnnonceurPanel().afficher(text);
			
		if(this.combattreController.guerrierGagneLaPartie()) this.laPartiePrendFin();
		
		else this.laPartieContinue();
	}

	
	private void laPartieContinue()
	{
		if (this.combattreController.estMort() == "monstre" && this.ouvrirCoffreController.typeButin() == "or") 
		{
			this.stockageContainers.getPossessionsGuerrier().setOr(this.ouvrirCoffreController.orGuerrier());
		}
		
		this.fflush();
		this.stockageContainers.getBarreActionPanel().setActions(this.actionsSuivantes("attaquer"));

		majBarreDeVieGuerrier();
		majBarreDeProtectionGuerrier();
		majBarreDeVieMonstre();
	}
	
	private void laPartiePrendFin()
	{
		this.fflush();
		this.stockageContainers.getBarreActionPanel().setActions(this.actionsSuivantes("creation guerrier"));
	}

	private void majBarreDeVieGuerrier() {
		/// \brief : met à jour la barre de vie du guerrier
		/// \return void
		this.stockageContainers.getInfosGuerrierPanel().setHealthMax(this.combattreController.getGuerrierHealthMax());
		this.stockageContainers.getInfosGuerrierPanel().setHealthQt(this.combattreController.getGuerrierHealth());
	}

	private void majBarreDeVieMonstre() {
		/// \brief : met à jour la barre de vie du monstre
		/// \return void
		this.stockageContainers.getInfosMonstre().setMonsterMaxHealth(this.combattreController.getMonstreHealthMax());
		this.stockageContainers.getInfosMonstre().setMonsterCurrentHealth(this.combattreController.getMonstreHealth());
	}

	private void majBarreDeProtectionGuerrier() {
		/// \brief : met à jour la barre de protection du guerrier si possible
		/// \return void
		if (this.deplacerGuerrierController.getLabyrinthe().getGuerrier().getArmure() != null) {
			this.stockageContainers.getInfosGuerrierPanel()
					.setShieldQtMax(this.combattreController.getGuerrierShieldMax());
			this.stockageContainers.getInfosGuerrierPanel().setShieldQt(this.combattreController.getGuerrierShield());
		}
	}

	private void autresActions() {
		/* rien pour l'instant */
	}

	private void actionDeplacement(String dirrection) {
		/// \brief : met en oeuvre l'action de 'deplacement'
		/// \param[in] dirrection : la dirrection du deplacement
		/// \return void

		/* Preaparations affichage */
		this.fflush(); // on vide la barre d'action
		this.stockageContainers.getAnnonceurPanel().cleanJTextArea(); // on vide les commentaires de l'annonceur

		/* action */
		String text = this.deplacerGuerrierController.deplacerGuerrier(dirrection); // on déplace le guerrier

		/* affichage */
		if (this.combattreController.combatPossible())
			this.majBarreDeVieMonstre();
		this.stockageContainers.getAnnonceurPanel().afficher(text);
		this.stockageContainers.getAnnonceurPanel().afficherTitre(this.deplacerGuerrierController.salleCourante());
		this.stockageContainers.getBarreActionPanel().setActions(this.actionsSuivantes("deplacement")); // on met à jour
																										// la barre
																										// d'action
	}

	private void fflush() {
		/// \brief : vide la barre d'action
		/// \return void
		String[] actionsTemp = { "", "", "", "", "", "" };
		this.stockageContainers.getBarreActionPanel().setActions(actionsTemp);
	}

	private String[] actionsSuivantes(String action) {
		/// \brief : renvoie une liste d'actions selon la situation actuelle du jeu
		/// \return la liste d'action au format String[]

		if (action.equals("creation guerrier"))
			return (new String[] { "valider", "quitter" });
		else if (action.equals("attaquer")) {
			if (this.combattreController.estMort() == "monstre" && this.ouvrirCoffreController.typeButin() != "or")
				return (new String[] { "non", "oui" });
		}
		if (this.combattreController.combatPossible())
			return (new String[] { "fuir", "attaquer", "equipement", "sauvegarder", "quitter" });
		else
			return (new String[] { "se déplacer", "equipement", "sauvegarder", "quitter" });

	}

}
