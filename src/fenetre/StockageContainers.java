package fenetre;

public class StockageContainers 
{
	/***************** attributs ********************/
	private Fenetre fenetre;
	private AnnonceurPanel annonceurPanel; // un panel d'annonceur qui affiche des messages
	private BarreActionPanel barreActionPanel; // le panel des actions possibles
	private DemanderNomPanel demanderNomPanel;
	private InfosGuerrierPanel infosGuerrierPanel;
	private InfosMonstre infosMonstre;
	private RemplacementInfosMonstre remplacementInfosMonstre;
	private PossessionsGuerrier possessionsGuerrier;

	/**************** constructeur *****************/
	public StockageContainers(FenetreManager fenetreController) 
	{
		this.fenetre = new Fenetre();
		this.possessionsGuerrier = new PossessionsGuerrier();
		this.annonceurPanel = new AnnonceurPanel(this.possessionsGuerrier); // un panel d'annonceur qui affiche des
																			// messages
		this.barreActionPanel = new BarreActionPanel(fenetreController); // le panel des actions possibles
		this.demanderNomPanel = new DemanderNomPanel();
		this.infosGuerrierPanel = new InfosGuerrierPanel();
		this.infosMonstre = new InfosMonstre();
		this.remplacementInfosMonstre = new RemplacementInfosMonstre();
	}

	/*************** gets ******************/
	public InfosMonstre getInfosMonstre() {
		return infosMonstre;
	}

	public Fenetre getFenetre() {
		return fenetre;
	}

	public AnnonceurPanel getAnnonceurPanel() {
		return annonceurPanel;
	}

	public BarreActionPanel getBarreActionPanel() {
		return barreActionPanel;
	}

	public DemanderNomPanel getDemanderNomPanel() {
		return demanderNomPanel;
	}

	public InfosGuerrierPanel getInfosGuerrierPanel() {
		return infosGuerrierPanel;
	}

	public PossessionsGuerrier getPossessionsGuerrier() {
		return possessionsGuerrier;
	}

	public RemplacementInfosMonstre getRemplacementInfosMonstre() {
		return remplacementInfosMonstre;
	}

}
