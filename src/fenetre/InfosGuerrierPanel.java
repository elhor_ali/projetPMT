package fenetre;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JProgressBar;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;

public class InfosGuerrierPanel extends JSplitPane {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JProgressBar health;
	private JProgressBar shield;

	public InfosGuerrierPanel() {
		super(HORIZONTAL_SPLIT);
		this.setPreferredSize(new Dimension(40, 320));
		this.setResizeWeight(0.5);
		this.setEnabled(false);
		this.setDividerSize(0);
		this.setBorder(null);
		this.initShield();
		this.initHealth();
		this.adds();
	}

	public void initShield() {
		shield = new JProgressBar(SwingConstants.VERTICAL, 0, 5);
		shield.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2, true));
		shield.setForeground(new Color(216, 175, 16));
		shield.setToolTipText("SHIELD");
		shield.setBackground(Color.BLACK);

	}

	public void initHealth() {

		health = new JProgressBar(SwingConstants.VERTICAL, 0, 10);
		health.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2, true));
		health.setForeground(new Color(59, 201, 56));
		health.setToolTipText("HP");
		health.setBackground(Color.BLACK);
	}

	public void adds() {
		this.add(this.shield);
		this.add(health);
	}

	public void setHealthQt(int quantite) {
		this.health.setValue(quantite);
	}

	public void setHealthMax(int quantite) {
		this.health.setMaximum(quantite);
	}

	public void setShieldQt(int quantite) {
		this.shield.setValue(quantite);
	}

	public void setShieldQtMax(int quantite) {
		this.shield.setMaximum(quantite);
	}
}
