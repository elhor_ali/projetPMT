package fenetre;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;

/* la fenetre du jeu */
public class Fenetre extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BorderLayout borderLayout;

	public Fenetre() {
		this.setPreferredSize(new Dimension(900, 500)); // definir les dimensions
		this.pack(); // important pour packer
		this.setLocationRelativeTo(null); // centrer par rapport à l'écran
		this.getContentPane().setBackground(Color.BLACK); // mettre un font noir car c'est classe
		this.borderLayout = new BorderLayout();
		this.getContentPane().setLayout(borderLayout);
		this.initPixelFont();
	}

	public void setConfiguration(Container container) {
		getContentPane().removeAll();
		getContentPane().add(container, BorderLayout.CENTER);
		this.repaint();
	}

	public void setConfiguration(Container container1, Container container2) {
		getContentPane().removeAll();
		getContentPane().add(container2, BorderLayout.SOUTH);
		getContentPane().add(container1, BorderLayout.CENTER);
		this.repaint();
	}

	public void setConfiguration(Container container1, Container container2, Container container3) {
		getContentPane().removeAll();
		getContentPane().add(container3, BorderLayout.EAST);
		getContentPane().add(container2, BorderLayout.SOUTH);
		getContentPane().add(container1, BorderLayout.CENTER);
		this.repaint();
	}

	public void setConfiguration(Container container1, Container container2, Container container3,
			Container container4) {
		getContentPane().removeAll();
		getContentPane().add(container4, BorderLayout.WEST);
		getContentPane().add(container3, BorderLayout.EAST);
		getContentPane().add(container2, BorderLayout.SOUTH);
		getContentPane().add(container1, BorderLayout.CENTER);
		this.repaint();
	}

	public void changeLeftComponent(Container container) {
		if (!borderLayout.getLayoutComponent(BorderLayout.WEST).equals(container)) {
			this.remove(borderLayout.getLayoutComponent(BorderLayout.WEST));
			getContentPane().add(container, BorderLayout.WEST);
			this.repaint();
		}
	}

	public void initPixelFont() {
		try {
			Font pixel = Font.createFont(Font.TRUETYPE_FONT, new File("ressources/fonts/SF Pixelate.ttf"));
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(pixel);
		} catch (IOException | FontFormatException e) {
			// Handle exception
		}
	}

}
