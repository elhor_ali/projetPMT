package fenetre;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;

public class RemplacementInfosMonstre extends JPanel 
{
	/**
	 * 
	 */
	
	/********* attribut *************/
	private static final long serialVersionUID = 1L;

	/************ constructeur ************/
	public RemplacementInfosMonstre() 
	{
		super();
		this.setPreferredSize(new Dimension(20, 320));
		this.setBackground(Color.BLACK);
	}
}
