package fenetre;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class PossessionsGuerrier 
extends JPanel 
{
	/***************** attributs *****************/
	private static final long serialVersionUID = 1L;

	private JLabel coins;
	private JLabel armor;
	private JLabel weapon;

	/**************** constructeur ****************/
	public PossessionsGuerrier() 
	{
		this.setPreferredSize(new Dimension(900, 40));
		this.setBackground(Color.BLACK);
		this.setBorder(null);
		FlowLayout flowLayout = new FlowLayout();
		this.setLayout(flowLayout);
		flowLayout.setHgap(150);
		this.setVisible(true);

		initCoins();
		initArmor();
		initWeapon();

		this.add(weapon, FlowLayout.LEFT);
		this.add(armor, FlowLayout.CENTER);
		this.add(coins, FlowLayout.RIGHT);
	}

	/************* initialisations **************/
	public void initCoins() 
	{
		this.coins = new JLabel();
		this.coins.setBackground(Color.BLACK); // la couleur du fond
		this.coins.setForeground(Color.WHITE); // la couleur de la police
		this.coins.setFont(new Font("SF Pixelate", Font.PLAIN, 20));
		this.coins.setHorizontalAlignment(SwingConstants.CENTER);
		this.coins.setIcon(new ImageIcon("ressources/icons/coin.png"));
		this.coins.setText("0");
	}

	public void initArmor() {
		this.armor = new JLabel();
		this.armor.setBackground(Color.BLACK); // la couleur du fond
		this.armor.setForeground(Color.WHITE); // la couleur de la police
		this.armor.setFont(new Font("SF Pixelate", Font.PLAIN, 20));
		this.armor.setHorizontalAlignment(SwingConstants.CENTER);
		this.armor.setIcon(new ImageIcon("ressources/icons/armures/0.png"));
		this.armor.setText("(0)");
	}

	public void initWeapon() {

		this.weapon = new JLabel();
		this.weapon.setBackground(Color.BLACK); // la couleur du fond
		this.weapon.setForeground(Color.WHITE); // la couleur de la police
		this.weapon.setFont(new Font("SF Pixelate", Font.PLAIN, 20));
		this.weapon.setHorizontalAlignment(SwingConstants.CENTER);
		this.weapon.setIcon(new ImageIcon("ressources/icons/armes/0.png"));
		this.weapon.setText("(0)");
	}

	/********************** sets **********************/
	public void setOr(int somme) 
	{
		/// \brief : permet de changer la some d'or du guerrier à l'ecran 
		/// \param[in] somme : la nouvelle somme d'or detenue
		/// \return : void
		this.coins.setText(String.valueOf(somme));
	}

	public void setArme(int rarete) 
	{
		/// \brief : permet de modifier le logo et la rarete de l'arme à l'ecran 
		/// \param[in] rarete : la rarete de l'arme
		/// \return : void
		Color[] couleurs = { new Color(220, 220, 220), new Color(115, 176, 98), new Color(29, 37, 111),
				new Color(155, 21, 72), new Color(231, 154, 14) };
		this.weapon.setIcon(new ImageIcon("ressources/icons/armes/" + rarete + ".png"));
		this.weapon.setForeground(couleurs[rarete - 1]);
		this.weapon.setText("(" + rarete + ")");
	}

	public void setArmure(int rarete) 
	{
		/// \brief : permet de modifier le logo et la rarete de l'armure à l'ecran 
		/// \param[in] rarete : la rarete de l'armure
		/// \return : void
		Color[] couleurs = { new Color(220, 220, 220), new Color(115, 176, 98), new Color(29, 37, 111),
				new Color(155, 21, 72), new Color(231, 154, 14) };
		this.armor.setIcon(new ImageIcon("ressources/icons/armures/" + rarete + ".png"));
		this.armor.setForeground(couleurs[rarete - 1]);
		this.armor.setText("(" + rarete + ")");
	}

}
