package coffres;

public class Armure extends Butin {
	private int puissanceMax;
	private int puissance;

	public Armure(int rarete) {
		super(rarete);
		this.puissance = rarete;
		this.puissanceMax = rarete;
	}

	public int getPuissance() {
		return puissance;
	}

	public int getPuissanceMax() {
		return puissanceMax;
	}

	public void setPuissance(int puissance) {
		this.puissance = puissance;
	}

}
