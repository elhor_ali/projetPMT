package coffres;

public abstract class Butin {
	int rarete;

	public Butin(int rarete) {
		this.rarete = rarete;
	}

	public int getRarete() {
		return rarete;
	}

	public TypeButin getTypeButin() {
		if (this instanceof Arme)
			return TypeButin.ARME;
		else if (this instanceof Armure)
			return TypeButin.ARMURE;
		else
			return TypeButin.OR;
	}

}
