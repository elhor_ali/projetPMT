package coffres;

public class Arme extends Butin {
	private int puissanceMax;
	private int puissance;

	public Arme(int rarete) {
		super(rarete);
		this.puissance = rarete;
		this.puissanceMax = rarete;
	}

	public int getPuissance() {
		return puissance;
	}

	public int getPuissanceMax() {
		return puissanceMax;
	}

	public void setPuissance(int puissance) {
		this.puissance = puissance;
	}

}
