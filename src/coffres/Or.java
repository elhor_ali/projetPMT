package coffres;

import java.util.Random;

public class Or extends Butin {
	private int quantite;

	public Or(int rarete) {
		super(rarete);
		this.quantite = genererOrQt();
	}

	public int genererOrQt() {
		return generateNumber(this.rarete * 10, (this.rarete + 1) * 10);
	}

	public int generateNumber(int min, int max) {
		return new Random().nextInt(max - min + 1) + min;
	}

	public int getQuantite() {
		return quantite;
	}

}
