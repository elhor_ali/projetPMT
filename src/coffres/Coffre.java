package coffres;

import java.util.Random;

public class Coffre {
	private Butin butin;

	public Coffre() {
		genererButin();
	}

	public Butin getButin() {
		return butin;
	}

	public void genererButin() {
		int choix = generateNumber(0, 9);
		int rarete = generateNumber(1, 5);
		if (choix == 0)
			butin = new Arme(rarete);
		else if (choix == 1)
			butin = new Armure(rarete);
		else
			butin = new Or(rarete);
	}

	public int generateNumber(int min, int max) {
		return new Random().nextInt(max - min + 1) + min;
	}

}
