package loot;

import java.util.Random;

public class Or 
extends Butin 
{
	/********* attribut **********/
	private int quantite;

	/********* constructeur ********/
	public Or(int rarete) 
	{
		super(rarete);
		this.quantite = genererOrQt();
	}

	/********** get *********/
	public int getQuantite() 
	{
		return quantite;
	}
	
	/********** divers *********/
	public int genererOrQt() 
	{
		/// \brief : permet de generer une quantité d'or selon la rarete
		/// \return : une entier correpondant à la quantité d'or
		return generateNumber(this.rarete * 10, (this.rarete + 1) * 10);
	}

	public int generateNumber(int min, int max) 
	{
		/// \brief : permet de generer un nombre entre min et max
		/// \param[in] min : le nombre min
		/// \param[in] max : le nombre max
		/// \return : une entier entre min et max
		return new Random().nextInt(max - min + 1) + min;
	}


}
