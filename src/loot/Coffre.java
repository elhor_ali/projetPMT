package loot;

import java.util.Random;

public class Coffre 
{
	/*********** attribut *************/
	private Butin butin;

	/*********** constructeur ***********/
	public Coffre() 
	{
		genererButin();
	}

	/************* get *************/
	public Butin getButin() {
		return butin;
	}

	/************* divers ************/
	public void genererButin() 
	{
		/// \brief : genere un butin (soit arme, soit or, soit armure)
		/// \return : void
		int choix = generateNumber(0, 9);
		int rarete = generateNumber(1, 5);
		if (choix == 0)
			butin = new Arme(rarete);
		else if (choix == 1)
			butin = new Armure(rarete);
		else
			butin = new Or(rarete);
	}

	public int generateNumber(int min, int max) 
	{
		/// \brief : permet de generer un nombre entre min et max
		/// \param[in] min : le nombre min
		/// \param[in] max : le nombre max
		/// \return : une entier entre min et max
		return new Random().nextInt(max - min + 1) + min;
	}

}
