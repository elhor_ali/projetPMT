package loot;

public abstract class Butin 
{
	/********** attributs ***********/
	protected int rarete;

	/*********** constructeur ***********/
	public Butin(int rarete) {
		this.rarete = rarete;
	}

	/************* gets *************/
	public int getRarete() {
		return rarete;
	}

	public TypeButin getTypeButin() 
	{
		if (this instanceof Arme)
			return TypeButin.ARME;
		else if (this instanceof Armure)
			return TypeButin.ARMURE;
		else
			return TypeButin.OR;
	}

}
