package loot;

public class Arme 
extends Butin 
{
	/********** attributs **********/
	private int puissanceMax;
	private int puissance;

	/*********** constructeur **********/
	public Arme(int rarete) 
	{
		super(rarete);
		this.puissance = rarete;
		this.puissanceMax = rarete;
	}

	/*************** gets *************/
	public int getPuissance() {
		return puissance;
	}

	public int getPuissanceMax() {
		return puissanceMax;
	}

	
	/************ set ***********/
	public void setPuissance(int puissance) {
		this.puissance = puissance;
	}

}
